﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITeacherRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Teacher> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        Teacher GetById(long teacherId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        List<Teacher> GetByFilter(string filter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newTeacher"></param>
        /// <returns></returns>
        Teacher Add(Teacher newTeacher);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacher"></param>
        /// <returns></returns>
        bool Update(Teacher teacher);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        bool Delete(long teacherId);
    }
}
