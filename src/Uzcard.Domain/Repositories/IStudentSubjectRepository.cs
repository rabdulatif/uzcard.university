﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStudentSubjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<StudentSubject> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        StudentSubject GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        List<StudentSubject> GetExcellentStudents(long subjectId);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        StudentSubject Add(StudentSubject obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(StudentSubject obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
