﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICityRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<City> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        City GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newCity"></param>
        /// <returns></returns>
        City Add(City newCity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        bool Update(City city);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        bool Delete(long cityId);
    }
}
