﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITeacherSubjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<TeacherSubject> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TeacherSubject GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Teacher> GetHighestLowestMarkStudents();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        TeacherSubject Add(TeacherSubject obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(TeacherSubject obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
