﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDepartmentRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Department> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Department GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newDepartment"></param>
        /// <returns></returns>
        Department Add(Department newDepartment);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        bool Update(Department department);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        bool Delete(long departmentId);
    }
}
