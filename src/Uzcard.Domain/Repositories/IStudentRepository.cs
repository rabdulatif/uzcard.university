﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStudentRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Student> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        Student GetById(long studentId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        List<Student> GetByFilter(string filter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newStudent"></param>
        /// <returns></returns>
        Student Add(Student newStudent);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        bool Update(Student student);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        bool Delete(long studentId);
    }
}
