﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISubjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Subject> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Subject GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newSubject"></param>
        /// <returns></returns>
        Subject Add(Subject newSubject);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        bool Update(Subject subject);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        bool Delete(long subjectId);
    }
}
