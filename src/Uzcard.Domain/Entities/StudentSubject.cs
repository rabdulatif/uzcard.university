﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentSubject : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("studentsubjectid")]
        public long StudentSubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column("studentid")]
        public long StudentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(StudentId))]
        public virtual Student Student { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column("subjectid")]
        public long SubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(SubjectId))]
        public virtual Subject Subject { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("mark")]
        public int Mark { get; set; }
    }
}
