﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherSubject : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("teachersubjectid")]
        public long TeacherSubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column("teacherid")]
        public long TeacherId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(TeacherId))]
        public virtual Teacher Teacher { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column("subjectid")]
        public long SubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(SubjectId))]
        public virtual Subject Subject { get; set; }
    }
}
