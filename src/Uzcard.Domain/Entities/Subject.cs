﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Subject : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("subjectid")]
        public long SubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(30, MinimumLength = 3)]
        [Column("name")]
        public string Name { get; set; }
    }
}
