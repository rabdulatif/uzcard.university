﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Column("createdon")]
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("modifiedon")]
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("isdeleted")]
        public bool IsDeleted { get; set; }
    }
}
