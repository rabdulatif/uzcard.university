﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class City : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("cityid")]
        public long CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(30, MinimumLength = 3)]
        [Column("name")]
        public string Name { get; set; }
    }
}
