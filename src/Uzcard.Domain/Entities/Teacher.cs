﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Teacher : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("teacherid")]
        public long TeacherId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(30, MinimumLength = 3)]
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("cityid")]
        public long? CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(CityId))]
        public virtual City City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column("birthdate")]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("gender")]
        public string Gender { get; set; }
    }
}
