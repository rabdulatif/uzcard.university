﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Student : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("studentid")]
        public long StudentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(30, MinimumLength = 3)]
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("cityid")]
        public long? CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(CityId))]
        public virtual City City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Column("birthdate")]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("gender")]
        public string Gender { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("currentgradelevel")]
        public int CurrentGradeLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("departmentid")]
        public long? DepartmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(DepartmentId))]
        public virtual Department Department { get; set; }
    }
}
