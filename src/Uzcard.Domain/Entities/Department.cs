﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Uzcard.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Department : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("departmentid")]
        public long DepartmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(30, MinimumLength = 4)]
        [Column("name")]
        public string Name { get; set; }
    }
}
