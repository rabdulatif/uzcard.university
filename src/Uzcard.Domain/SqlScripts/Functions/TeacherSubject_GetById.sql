﻿CREATE OR REPLACE FUNCTION  TeacherSubject_GetById(id bigint)
returns table (
    teachersubjectid bigint,
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean,
    teacherid bigint,
    subjectid bigint
) as $$
begin
return query 

SELECT 
	teacher.teachersubjectid,
	teacher.createdon,
	teacher.modifiedon,
	teacher.isdeleted, 
	teacher.teacherid,
	teacher.subjectid
FROM public."TeacherSubjects" as teacher
WHERE teacher.isdeleted=false
AND teacher.teachersubjectid=id;

end; 
$$ LANGUAGE plpgsql;