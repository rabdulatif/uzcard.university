﻿CREATE OR REPLACE FUNCTION  TeacherSubject_GetAll()
returns table (
    "TeacherSubjectId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "TeacherId" bigint,
    "SubjectId" bigint
) as $$
begin
return query 

SELECT 
	teacher.teachersubjectid,
	teacher.createdon,
	teacher.modifiedon,
	teacher.isdeleted, 
	teacher.teacherid,
	teacher.subjectid
FROM public."TeacherSubjects" as teacher
WHERE teacher.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;