﻿CREATE OR REPLACE FUNCTION  Students_GetObjectByFilter(filterStr character)
returns table (
    "StudentId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "Name" character varying(30),
    "CityId" bigint,
    "BirthDate" timestamp without time zone,
    "Gender" text,
    "CurrentGradeLevel" integer,
    "DepartmentId" bigint
) as $$
begin
return query 

SELECT 
student.studentid, 
student.createdon, 
student.modifiedon,
student.isdeleted, 
student.name, 
student.cityid, 
student.birthdate,
student.gender, 
student.currentgradelevel, 
student.departmentid

FROM public."Students" as student
INNER JOIN public."Cities" as city
	ON student.cityid=city.cityid
INNER JOIN public."Departments" as department
	ON student.departmentid = department.departmentid
WHERE student.name like '%'||filterStr||'%'
OR student.gender like '%'||filterStr||'%'
OR city.name like '%'||filterStr||'%'
OR department.name like '%'||filterStr||'%'
AND student.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;