﻿CREATE OR REPLACE FUNCTION Department_GetById(id bigint)
returns table (
    departmentid bigint,
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean,
    name character varying(30)
) as $$
begin
return query 

SELECT 
department.departmentid, 
department.createdon, 
department.modifiedon,
department.isdeleted, 
department.name
FROM public."Departments" as department
WHERE department.isdeleted=false
AND department.departmentid=id;

end; 
$$ LANGUAGE plpgsql;