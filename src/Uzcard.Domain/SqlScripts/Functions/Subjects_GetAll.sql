﻿CREATE OR REPLACE FUNCTION  Subjects_GetAll()
returns table (
    "SubjectId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "Name" character varying(30)
) as $$
begin
return query 

SELECT 
	subjects.subjectid,
	subjects.createdon,
	subjects.modifiedon,
	subjects.isdeleted, 
	subjects.name
FROM public."Subjects" as subjects
WHERE subjects.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;