﻿CREATE OR REPLACE FUNCTION City_GetById(id bigint)
returns table (
    cityid bigint,
    name character varying(30),
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean
) as $$
begin
return query 

SELECT 
city.cityid, 
city.name,
city.createdon, 
city.modifiedon,
city.isdeleted 
FROM public."Cities" as city
WHERE city.isdeleted=false
AND city.cityid = id;

end; 
$$ LANGUAGE plpgsql;