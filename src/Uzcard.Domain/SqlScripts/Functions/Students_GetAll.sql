﻿--CREATE OR REPLACE FUNCTION public.students_getall(
--	)
--    RETURNS TABLE(students "Students", cities "Cities") 
--    LANGUAGE 'plpgsql'
--    COST 100
--    VOLATILE PARALLEL UNSAFE
--    ROWS 1000

--AS $BODY$
--begin
--return query 

--SELECT st, ct
--FROM public."Students" st
--inner join public."Cities" ct
--on st.cityid=ct.cityid
--WHERE st.isdeleted=false;

--end;
--$BODY$;
CREATE OR REPLACE FUNCTION Students_GetAll()
returns table (
    "StudentId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "Name" character varying(30),
    "CityId" bigint,
    "BirthDate" timestamp without time zone,
    "Gender" text,
    "CurrentGradeLevel" integer,
    "DepartmentId" bigint
) as $$
begin
return query 

SELECT 
student.studentid, 
student.createdon, 
student.modifiedon,
student.isdeleted, 
student.name, 
student.cityid, 
student.birthdate,
student.gender, 
student.currentgradelevel, 
student.departmentid

FROM public."Students" as student
WHERE student.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;