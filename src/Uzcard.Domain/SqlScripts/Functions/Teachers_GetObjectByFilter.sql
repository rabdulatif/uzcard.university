﻿CREATE OR REPLACE FUNCTION  Teachers_GetObjectByFilter(filterStr character)
returns table (
    "TeacherId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "Name" character varying(30),
    "CityId" bigint,
    "BirthDate" timestamp without time zone ,
    "Gender" text
) as $$
begin
return query 

SELECT 
	teacher.teacherid,
	teacher.createdon,
	teacher.modifiedon,
	teacher.isdeleted, 
	teacher.name,
	teacher.cityid,
	teacher.birthdate,
	teacher.gender
FROM public."Teachers" as teacher
INNER JOIN public."Cities" as city
	ON teacher.cityid = city.cityid
WHERE teacher.name like '%'||filterStr||'%'
OR teacher.gender like '%'||filterStr||'%'
OR city.name like '%'||filterStr||'%'
AND teacher.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;
