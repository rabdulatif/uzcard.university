﻿CREATE OR REPLACE FUNCTION StudentSubject_GetAll()
returns table (
    "StudentSubjectId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "StudentId" bigint,
    "SubjectId" bigint,
    "Mark" integer
) as $$
begin
return query 

SELECT 
studentSubject.studentsubjectid, 
studentSubject.createdon, 
studentSubject.modifiedon,
studentSubject.isdeleted, 
studentSubject.studentid, 
studentSubject.subjectid, 
studentSubject.mark

FROM public."StudentSubjects" as studentSubject
WHERE studentSubject.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;