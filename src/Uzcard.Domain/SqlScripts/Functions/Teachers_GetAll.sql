﻿CREATE OR REPLACE FUNCTION  Teachers_GetAll()
returns table (
    "TeacherId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "Name" character varying(30),
    "CityId" bigint,
    "BirthDate" timestamp without time zone ,
    "Gender" text
) as $$
begin
return query 

SELECT 
	teacher.teacherid,
	teacher.createdon,
	teacher.modifiedon,
	teacher.isdeleted, 
	teacher.name,
	teacher.cityid,
	teacher.birthdate,
	teacher.gender
FROM public."Teachers" as teacher
WHERE teacher.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;