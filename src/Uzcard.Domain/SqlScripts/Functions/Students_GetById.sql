﻿CREATE OR REPLACE FUNCTION  Students_GetById(id bigint)
returns table (
    studentid bigint,
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean,
    name character varying(30),
    cityid bigint,
    birthdate timestamp without time zone,
    gender text,
    currentgradelevel integer,
    departmentid bigint
) as $$
begin
return query 

SELECT 
student.studentid, 
student.createdon, 
student.modifiedon,
student.isdeleted, 
student.name, 
student.cityid, 
student.birthdate,
student.gender, 
student.currentgradelevel, 
student.departmentid

FROM public."Students" as student
WHERE student.studentid=id
AND student.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;