﻿CREATE OR REPLACE FUNCTION Department_GetAll()
returns table (
    "DepartmentId" bigint,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean,
    "Name" character varying(30)
) as $$
begin
return query 

SELECT 
department.departmentid, 
department.createdon, 
department.modifiedon,
department.isdeleted, 
department.name
FROM public."Departments" as department
WHERE department.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;