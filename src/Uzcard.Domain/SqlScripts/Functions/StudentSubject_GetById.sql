﻿CREATE OR REPLACE FUNCTION StudentSubject_GetById(id bigint)
returns table (
    studentsubjectid bigint,
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean,
    studentid bigint,
    subjectid bigint,
    mark integer
) as $$
begin
return query 

SELECT 
studentSubject.studentsubjectid, 
studentSubject.createdon, 
studentSubject.modifiedon,
studentSubject.isdeleted, 
studentSubject.studentid, 
studentSubject.subjectid, 
studentSubject.mark

FROM public."StudentSubjects" as studentSubject
WHERE studentSubject.isdeleted=false
AND studentSubject.studentsubjectid=id;

end; 
$$ LANGUAGE plpgsql;