﻿CREATE OR REPLACE FUNCTION  Teachers_GetById(id bigint)
returns table (
    teacherid bigint,
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean,
    name character varying(30),
    cityid bigint,
    birthdate timestamp without time zone ,
    gender text
) as $$
begin
return query 

SELECT 
	teacher.teacherid,
	teacher.createdon,
	teacher.modifiedon,
	teacher.isdeleted, 
	teacher.name,
	teacher.cityid,
	teacher.birthdate,
	teacher.gender
FROM public."Teachers" as teacher
WHERE teacher.teacherid=id
AND teacher.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;