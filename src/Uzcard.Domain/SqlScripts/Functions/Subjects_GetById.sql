﻿CREATE OR REPLACE FUNCTION  Subjects_GetById(id bigint)
returns table (
    subjectid bigint,
    createdon timestamp without time zone,
    modifiedon timestamp without time zone,
    isdeleted boolean,
    name character varying(30)
) as $$
begin
return query 

SELECT 
	subjects.subjectid,
	subjects.createdon,
	subjects.modifiedon,
	subjects.isdeleted, 
	subjects.name
FROM public."Subjects" as subjects
WHERE subjects.isdeleted=false
AND subjects.subjectid=id;

end; 
$$ LANGUAGE plpgsql;