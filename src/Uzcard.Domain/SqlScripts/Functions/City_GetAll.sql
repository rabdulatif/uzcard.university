﻿CREATE OR REPLACE FUNCTION City_GetAll()
returns table (
    "CityId" bigint ,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "IsDeleted" boolean ,
    "Name" character varying(30)
) as $$
begin
return query 

SELECT 
city.cityid, 
city.createdon, 
city.modifiedon,
city.isdeleted, 
city.name
FROM public."Cities" as city
WHERE city.isdeleted=false;

end; 
$$ LANGUAGE plpgsql;