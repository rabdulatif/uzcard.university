﻿using System;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// DTO
    /// </summary>
    public class AuditApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ModifiedOn { get; set; }

    }
}
