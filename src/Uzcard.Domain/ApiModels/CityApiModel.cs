﻿using System;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class CityApiModel : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public long CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
