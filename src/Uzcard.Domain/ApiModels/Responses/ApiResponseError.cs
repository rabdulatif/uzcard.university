﻿namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiResponseError
    {
        /// <summary>
        /// 
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
