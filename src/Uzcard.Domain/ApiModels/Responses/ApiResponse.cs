﻿namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResponse<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApiResponseError Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApiResponse()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public ApiResponse(T result)
        {
            Result = result;
            IsSuccess = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public static implicit operator ApiResponse<T>(T result)
        {
            return new ApiResponse<T>(result);
        }
    }
}
