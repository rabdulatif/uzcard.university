﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class SubjectApiModel : AuditApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(30, MinimumLength = 3)]
        public string Name { get; set; }
    }
}
