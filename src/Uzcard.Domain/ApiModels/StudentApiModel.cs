﻿using System;
using System.ComponentModel.DataAnnotations;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// DTO
    /// </summary>
    public class StudentApiModel : AuditApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CityApiModel City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Gender { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CurrentGradeLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? DepartmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DepartmentApiModel Department { get; set; }
    }
}
