﻿using System;
using System.ComponentModel.DataAnnotations;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherApiModel : AuditApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long TeacherId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CityApiModel City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Gender { get; set; }
    }
}
