﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentSubjectApiModel : AuditApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long StudentSubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long StudentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual StudentApiModel Student { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long SubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual SubjectApiModel Subject { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Mark { get; set; }
    }
}
