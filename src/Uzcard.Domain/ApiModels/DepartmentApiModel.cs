﻿using System;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class DepartmentApiModel : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public long DepartmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
