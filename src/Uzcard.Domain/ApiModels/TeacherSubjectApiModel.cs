﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uzcard.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherSubjectApiModel : AuditApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long TeacherSubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long TeacherId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual TeacherApiModel Teacher { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long SubjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual SubjectApiModel Subject { get; set; }
    }
}
