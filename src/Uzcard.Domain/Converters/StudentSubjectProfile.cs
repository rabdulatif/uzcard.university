﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentSubjectProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public StudentSubjectProfile()
        {
            CreateMap<StudentSubject, StudentSubjectApiModel>();
            CreateMap<StudentSubjectApiModel, StudentSubject>();
        }
    }
}
