﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public StudentProfile()
        {
            CreateMap<Student, StudentApiModel>();
            CreateMap<StudentApiModel, Student>();
        }
    }
}
