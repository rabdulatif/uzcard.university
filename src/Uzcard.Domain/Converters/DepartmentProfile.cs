﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class DepartmentProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public DepartmentProfile()
        {
            CreateMap<Department, DepartmentApiModel>();
            CreateMap<DepartmentApiModel, Department>();
        }
    }
}
