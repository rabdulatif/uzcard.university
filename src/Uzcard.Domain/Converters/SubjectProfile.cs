﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class SubjectProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public SubjectProfile()
        {
            CreateMap<Subject, SubjectApiModel>();
            CreateMap<SubjectApiModel, Subject>();
        }
    }
}
