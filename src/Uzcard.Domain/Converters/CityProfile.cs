﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class CityProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public CityProfile()
        {
            CreateMap<City, CityApiModel>();
            CreateMap<CityApiModel, City>();
        }
    }
}
