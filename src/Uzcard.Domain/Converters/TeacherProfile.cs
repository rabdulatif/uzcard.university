﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public TeacherProfile()
        {
            CreateMap<Teacher, TeacherApiModel>();
            CreateMap<TeacherApiModel, Teacher>();
        }
    }
}
