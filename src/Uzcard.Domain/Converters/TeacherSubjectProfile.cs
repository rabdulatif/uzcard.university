﻿using AutoMapper;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;

namespace Uzcard.Domain.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherSubjectProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public TeacherSubjectProfile()
        {
            CreateMap<TeacherSubject, TeacherSubjectApiModel>();
            CreateMap<TeacherSubjectApiModel, TeacherSubject>();
        }
    }
}
