﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Uzcard.DataEFCore.Configurations;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversityContext : DbContext
    {
        public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public UniversityContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<City> Cities { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<Department> Departments { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<Student> Students { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<Teacher> Teachers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<Subject> Subjects { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<StudentSubject> StudentSubjects { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<TeacherSubject> TeacherSubjects { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CityConfiguration());
            modelBuilder.ApplyConfiguration(new DepartmentConfiguration());
            modelBuilder.ApplyConfiguration(new StudentConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherConfiguration());
            modelBuilder.ApplyConfiguration(new SubjectConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherSubjectsConfiguration());
            modelBuilder.ApplyConfiguration(new StudentSubjectsConfiguration());
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        }
    }
}
