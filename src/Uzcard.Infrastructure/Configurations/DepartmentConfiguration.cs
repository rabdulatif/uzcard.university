﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.HasData
            (
                new Department
                {
                    DepartmentId = 1,
                    CreatedOn = DateTime.Now,
                    Name = "Department 1",
                },
                new Department
                {
                    DepartmentId =2,
                    CreatedOn = DateTime.Now,
                    Name = "Department 2",
                },
                new Department
                {
                    DepartmentId = 3,
                    CreatedOn = DateTime.Now,
                    Name = "Department 3",
                },
                new Department
                {
                    DepartmentId = 4,
                    CreatedOn = DateTime.Now,
                    Name = "Department 4",
                },
                new Department
                {
                    DepartmentId = 5,
                    CreatedOn = DateTime.Now,
                    Name = "Department 5",
                }
            );
        }
    }
}
