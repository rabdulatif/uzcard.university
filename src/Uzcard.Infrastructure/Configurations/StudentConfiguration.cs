﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.HasData
            (
                new Student
                {
                    StudentId = 1,
                    CityId = 1,
                    DepartmentId = 1,
                    CreatedOn = DateTime.Now,
                    Name = "Mike 1",
                    BirthDate = new DateTime(2000, 1, 1),
                    Gender = Enum.GetName(typeof(Gender), Gender.Female)
                },
                new Student
                {
                    StudentId = 2,
                    CityId = 2,
                    DepartmentId = 2,
                    CreatedOn = DateTime.Now,
                    Name = "Mike 2",
                    BirthDate = new DateTime(2000, 1, 1),
                    Gender = Enum.GetName(typeof(Gender), Gender.Male)
                },
                new Student
                {
                    StudentId = 3,
                    CityId = 1,
                    DepartmentId = 3,
                    CreatedOn = DateTime.Now,
                    Name = "Mike 3",
                    BirthDate = new DateTime(2000, 1, 1),
                    Gender = Enum.GetName(typeof(Gender), Gender.Female)
                },
                new Student
                {
                    StudentId = 4,
                    CityId = 4,
                    DepartmentId = 1,
                    CreatedOn = DateTime.Now,
                    Name = "Mike 3",
                    BirthDate = new DateTime(2000, 1, 1),
                    Gender = Enum.GetName(typeof(Gender), Gender.Male)
                },
                new Student
                {
                    StudentId = 5,
                    CityId = 5,
                    DepartmentId = 5,
                    CreatedOn = DateTime.Now,
                    Name = "Mike 4",
                    BirthDate = new DateTime(2000, 1, 1),
                    Gender = Enum.GetName(typeof(Gender), Gender.Male)
                }
            );
        }
    }
}
