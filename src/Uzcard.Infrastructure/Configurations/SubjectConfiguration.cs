﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class SubjectConfiguration : IEntityTypeConfiguration<Subject>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            builder.HasData
               (
                   new Subject
                   {
                       SubjectId = 1,
                       CreatedOn = DateTime.Now,
                       Name = "Subject 1"
                   },
                   new Subject
                   {
                       SubjectId = 2,
                       CreatedOn = DateTime.Now,
                       Name = "Subject 2"
                   },
                   new Subject
                   {
                       SubjectId = 3,
                       CreatedOn = DateTime.Now,
                       Name = "Subject 3"
                   },
                   new Subject
                   {
                       SubjectId = 4,
                       CreatedOn = DateTime.Now,
                       Name = "Subject 4"
                   },
                   new Subject
                   {
                       SubjectId = 5,
                       CreatedOn = DateTime.Now,
                       Name = "Subject 5"
                   }
               );
        }
    }
}
