﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherSubjectsConfiguration : IEntityTypeConfiguration<TeacherSubject>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<TeacherSubject> builder)
        {
            builder.HasData
               (
                   new TeacherSubject
                   {
                       TeacherSubjectId = 1,
                       TeacherId = 1,
                       CreatedOn = DateTime.Now,
                       SubjectId = 1
                   },
                   new TeacherSubject
                   {
                       TeacherSubjectId = 2,
                       TeacherId = 2,
                       CreatedOn = DateTime.Now,
                       SubjectId = 2
                   },
                   new TeacherSubject
                   {
                       TeacherSubjectId = 3,
                       TeacherId = 3,
                       CreatedOn = DateTime.Now,
                       SubjectId = 3
                   },
                   new TeacherSubject
                   {
                       TeacherSubjectId = 4,
                       TeacherId = 4,
                       CreatedOn = DateTime.Now,
                       SubjectId = 4
                   },
                   new TeacherSubject
                   {
                       TeacherSubjectId = 5,
                       TeacherId = 5,
                       CreatedOn = DateTime.Now,
                       SubjectId = 5,
                   }
               );
        }
    }
}
