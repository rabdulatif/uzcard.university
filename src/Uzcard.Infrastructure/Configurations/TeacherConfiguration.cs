﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherConfiguration : IEntityTypeConfiguration<Teacher>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder.HasData
               (
                   new Teacher
                   {
                       TeacherId = 1,
                       CityId = 1,
                       CreatedOn = DateTime.Now,
                       Name = "Teacher 1",
                       Gender = Enum.GetName(typeof(Gender), Gender.Male)
                   },
                   new Teacher
                   {
                       TeacherId = 2,
                       CityId = 2,
                       CreatedOn = DateTime.Now,
                       Name = "Teacher 2",
                       Gender = Enum.GetName(typeof(Gender), Gender.Female)
                   },
                   new Teacher
                   {
                       TeacherId = 3,
                       CityId = 1,
                       CreatedOn = DateTime.Now,
                       Name = "Teacher 3",
                       Gender = Enum.GetName(typeof(Gender), Gender.Male)
                   },
                   new Teacher
                   {
                       TeacherId = 4,
                       CityId = 4,
                       CreatedOn = DateTime.Now,
                       Name = "Teacher 4",
                       Gender = Enum.GetName(typeof(Gender), Gender.Female)
                   },
                   new Teacher
                   {
                       TeacherId = 5,
                       CityId = 5,
                       CreatedOn = DateTime.Now,
                       Name = "Teacher 5",
                       Gender = Enum.GetName(typeof(Gender), Gender.Male)
                   }
               );
        }
    }
}
