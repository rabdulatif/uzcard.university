﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentSubjectsConfiguration : IEntityTypeConfiguration<StudentSubject>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<StudentSubject> builder)
        {
            builder.HasData
               (
                   new StudentSubject
                   {
                       StudentSubjectId = 1,
                       SubjectId = 1,
                       StudentId = 1,
                       CreatedOn = DateTime.Now,
                       Mark = 5
                   },
                   new StudentSubject
                   {
                       StudentSubjectId = 2,
                       SubjectId = 2,
                       StudentId = 1,
                       CreatedOn = DateTime.Now,
                       Mark = 3
                   },
                   new StudentSubject
                   {
                       StudentSubjectId = 3,
                       SubjectId = 1,
                       StudentId = 2,
                       CreatedOn = DateTime.Now,
                       Mark = 4
                   },
                   new StudentSubject
                   {
                       StudentSubjectId = 4,
                       SubjectId = 2,
                       StudentId = 2,
                       CreatedOn = DateTime.Now,
                       Mark = 2
                   },
                   new StudentSubject
                   {
                       StudentSubjectId = 5,
                       SubjectId = 1,
                       StudentId = 4,
                       CreatedOn = DateTime.Now,
                       Mark = 5
                   }
               );
        }
    }
}
