﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Uzcard.Domain.Entities;

namespace Uzcard.DataEFCore.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.HasData
            (
                new City
                {
                    CityId = 1,
                    CreatedOn = DateTime.Now,
                    Name = "Tashkent",
                },
                new City
                {
                    CityId = 2,
                    CreatedOn = DateTime.Now,
                    Name = "Samarqand",
                },
                new City
                {
                    CityId = 3,
                    CreatedOn = DateTime.Now,
                    Name = "Bukhara",
                },
                new City
                {
                    CityId = 4,
                    CreatedOn = DateTime.Now,
                    Name = "Andijan",
                },
                new City
                {
                    CityId = 5,
                    CreatedOn = DateTime.Now,
                    Name = "Jizzakh",
                }
            );
        }
    }
}
