﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Uzcard.DataEFCore.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    cityid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.cityid);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    departmentid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.departmentid);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    subjectid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.subjectid);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    teacherid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    cityid = table.Column<long>(type: "bigint", nullable: true),
                    birthdate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    gender = table.Column<string>(type: "text", nullable: true),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.teacherid);
                    table.ForeignKey(
                        name: "FK_Teachers_Cities_cityid",
                        column: x => x.cityid,
                        principalTable: "Cities",
                        principalColumn: "cityid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    studentid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    cityid = table.Column<long>(type: "bigint", nullable: true),
                    birthdate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    gender = table.Column<string>(type: "text", nullable: true),
                    currentgradelevel = table.Column<int>(type: "integer", nullable: false),
                    departmentid = table.Column<long>(type: "bigint", nullable: true),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.studentid);
                    table.ForeignKey(
                        name: "FK_Students_Cities_cityid",
                        column: x => x.cityid,
                        principalTable: "Cities",
                        principalColumn: "cityid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_Departments_departmentid",
                        column: x => x.departmentid,
                        principalTable: "Departments",
                        principalColumn: "departmentid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TeacherSubjects",
                columns: table => new
                {
                    teachersubjectid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    teacherid = table.Column<long>(type: "bigint", nullable: false),
                    subjectid = table.Column<long>(type: "bigint", nullable: false),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherSubjects", x => x.teachersubjectid);
                    table.ForeignKey(
                        name: "FK_TeacherSubjects_Subjects_subjectid",
                        column: x => x.subjectid,
                        principalTable: "Subjects",
                        principalColumn: "subjectid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherSubjects_Teachers_teacherid",
                        column: x => x.teacherid,
                        principalTable: "Teachers",
                        principalColumn: "teacherid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentSubjects",
                columns: table => new
                {
                    studentsubjectid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    studentid = table.Column<long>(type: "bigint", nullable: false),
                    subjectid = table.Column<long>(type: "bigint", nullable: false),
                    mark = table.Column<int>(type: "integer", nullable: false),
                    createdon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    modifiedon = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    isdeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSubjects", x => x.studentsubjectid);
                    table.ForeignKey(
                        name: "FK_StudentSubjects_Students_studentid",
                        column: x => x.studentid,
                        principalTable: "Students",
                        principalColumn: "studentid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentSubjects_Subjects_subjectid",
                        column: x => x.subjectid,
                        principalTable: "Subjects",
                        principalColumn: "subjectid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "cityid", "createdon", "isdeleted", "modifiedon", "name" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 12, 16, 18, 9, 49, 874, DateTimeKind.Local).AddTicks(9439), false, null, "Tashkent" },
                    { 2L, new DateTime(2020, 12, 16, 18, 9, 49, 876, DateTimeKind.Local).AddTicks(286), false, null, "Samarqand" },
                    { 3L, new DateTime(2020, 12, 16, 18, 9, 49, 876, DateTimeKind.Local).AddTicks(324), false, null, "Bukhara" },
                    { 4L, new DateTime(2020, 12, 16, 18, 9, 49, 876, DateTimeKind.Local).AddTicks(327), false, null, "Andijan" },
                    { 5L, new DateTime(2020, 12, 16, 18, 9, 49, 876, DateTimeKind.Local).AddTicks(329), false, null, "Jizzakh" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "departmentid", "createdon", "isdeleted", "modifiedon", "name" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 12, 16, 18, 9, 49, 877, DateTimeKind.Local).AddTicks(7179), false, null, "Department 1" },
                    { 2L, new DateTime(2020, 12, 16, 18, 9, 49, 877, DateTimeKind.Local).AddTicks(7689), false, null, "Department 2" },
                    { 3L, new DateTime(2020, 12, 16, 18, 9, 49, 877, DateTimeKind.Local).AddTicks(7708), false, null, "Department 3" },
                    { 4L, new DateTime(2020, 12, 16, 18, 9, 49, 877, DateTimeKind.Local).AddTicks(7710), false, null, "Department 4" },
                    { 5L, new DateTime(2020, 12, 16, 18, 9, 49, 877, DateTimeKind.Local).AddTicks(7711), false, null, "Department 5" }
                });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "subjectid", "createdon", "isdeleted", "modifiedon", "name" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(962), false, null, "Subject 1" },
                    { 2L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(1446), false, null, "Subject 2" },
                    { 3L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(1458), false, null, "Subject 3" },
                    { 4L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(1460), false, null, "Subject 4" },
                    { 5L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(1461), false, null, "Subject 5" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "studentid", "birthdate", "cityid", "createdon", "currentgradelevel", "departmentid", "gender", "isdeleted", "modifiedon", "name" },
                values: new object[,]
                {
                    { 1L, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, new DateTime(2020, 12, 16, 18, 9, 49, 878, DateTimeKind.Local).AddTicks(6849), 0, 1L, "Female", false, null, "Mike 1" },
                    { 4L, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4L, new DateTime(2020, 12, 16, 18, 9, 49, 878, DateTimeKind.Local).AddTicks(8695), 0, 1L, "Male", false, null, "Mike 3" },
                    { 2L, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2L, new DateTime(2020, 12, 16, 18, 9, 49, 878, DateTimeKind.Local).AddTicks(8655), 0, 2L, "Male", false, null, "Mike 2" },
                    { 3L, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, new DateTime(2020, 12, 16, 18, 9, 49, 878, DateTimeKind.Local).AddTicks(8690), 0, 3L, "Female", false, null, "Mike 3" },
                    { 5L, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5L, new DateTime(2020, 12, 16, 18, 9, 49, 878, DateTimeKind.Local).AddTicks(8698), 0, 5L, "Male", false, null, "Mike 4" }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "teacherid", "birthdate", "cityid", "createdon", "gender", "isdeleted", "modifiedon", "name" },
                values: new object[,]
                {
                    { 1L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, new DateTime(2020, 12, 16, 18, 9, 49, 879, DateTimeKind.Local).AddTicks(5535), "Male", false, null, "Teacher 1" },
                    { 3L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, new DateTime(2020, 12, 16, 18, 9, 49, 879, DateTimeKind.Local).AddTicks(6486), "Male", false, null, "Teacher 3" },
                    { 2L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2L, new DateTime(2020, 12, 16, 18, 9, 49, 879, DateTimeKind.Local).AddTicks(6455), "Female", false, null, "Teacher 2" },
                    { 4L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4L, new DateTime(2020, 12, 16, 18, 9, 49, 879, DateTimeKind.Local).AddTicks(6490), "Female", false, null, "Teacher 4" },
                    { 5L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5L, new DateTime(2020, 12, 16, 18, 9, 49, 879, DateTimeKind.Local).AddTicks(6492), "Male", false, null, "Teacher 5" }
                });

            migrationBuilder.InsertData(
                table: "StudentSubjects",
                columns: new[] { "studentsubjectid", "createdon", "isdeleted", "mark", "modifiedon", "studentid", "subjectid" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 12, 16, 18, 9, 49, 881, DateTimeKind.Local).AddTicks(2497), false, 5, null, 1L, 1L },
                    { 2L, new DateTime(2020, 12, 16, 18, 9, 49, 881, DateTimeKind.Local).AddTicks(2928), false, 3, null, 1L, 2L },
                    { 5L, new DateTime(2020, 12, 16, 18, 9, 49, 881, DateTimeKind.Local).AddTicks(2943), false, 5, null, 4L, 1L },
                    { 3L, new DateTime(2020, 12, 16, 18, 9, 49, 881, DateTimeKind.Local).AddTicks(2939), false, 4, null, 2L, 1L },
                    { 4L, new DateTime(2020, 12, 16, 18, 9, 49, 881, DateTimeKind.Local).AddTicks(2941), false, 2, null, 2L, 2L }
                });

            migrationBuilder.InsertData(
                table: "TeacherSubjects",
                columns: new[] { "teachersubjectid", "createdon", "isdeleted", "modifiedon", "subjectid", "teacherid" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(6511), false, null, 1L, 1L },
                    { 3L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(6952), false, null, 3L, 3L },
                    { 2L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(6940), false, null, 2L, 2L },
                    { 4L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(6953), false, null, 4L, 4L },
                    { 5L, new DateTime(2020, 12, 16, 18, 9, 49, 880, DateTimeKind.Local).AddTicks(6955), false, null, 5L, 5L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Students_cityid",
                table: "Students",
                column: "cityid");

            migrationBuilder.CreateIndex(
                name: "IX_Students_departmentid",
                table: "Students",
                column: "departmentid");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjects_studentid",
                table: "StudentSubjects",
                column: "studentid");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjects_subjectid",
                table: "StudentSubjects",
                column: "subjectid");

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_cityid",
                table: "Teachers",
                column: "cityid");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubjects_subjectid",
                table: "TeacherSubjects",
                column: "subjectid");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubjects_teacherid",
                table: "TeacherSubjects",
                column: "teacherid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentSubjects");

            migrationBuilder.DropTable(
                name: "TeacherSubjects");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropTable(
                name: "Teachers");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
