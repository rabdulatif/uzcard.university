﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Uzcard.Domain;

namespace Uzcard.DataEFCore.Migrations
{
    public partial class Functions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            AddFunctions(migrationBuilder);
        }

        private void AddFunctions(MigrationBuilder migrationBuilder)
        {
            var getAllStudents = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Students_GetAll.sql");
            migrationBuilder.Sql(getAllStudents);

            var getByIdStudents = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Students_GetById.sql");
            migrationBuilder.Sql(getByIdStudents);

            var getStudentsByFilter = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Students_GetObjectByFilter.sql");
            migrationBuilder.Sql(getStudentsByFilter);

            var getAllTeacher = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Teachers_GetAll.sql");
            migrationBuilder.Sql(getAllTeacher);

            var getByIdTeachers = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Teachers_GetById.sql");
            migrationBuilder.Sql(getByIdTeachers);

            var getTeachersByFilter = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Teachers_GetObjectByFilter.sql");
            migrationBuilder.Sql(getTeachersByFilter);

            var getAllSubjects = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Subjects_GetAll.sql");
            migrationBuilder.Sql(getAllSubjects);

            var getByIdSubjects = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Subjects_GetById.sql");
            migrationBuilder.Sql(getByIdSubjects);

            var getAllStudentSubject = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.StudentSubject_GetAll.sql");
            migrationBuilder.Sql(getAllStudentSubject);

            var getByIdStudentSubject = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.StudentSubject_GetById.sql");
            migrationBuilder.Sql(getByIdStudentSubject);

            var getAllTeacherSubject = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.TeacherSubject_GetAll.sql");
            migrationBuilder.Sql(getAllTeacherSubject);

            var getByIdTeacherSubject = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.TeacherSubject_GetById.sql");
            migrationBuilder.Sql(getByIdTeacherSubject);

            var getAllCities = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.City_GetAll.sql");
            migrationBuilder.Sql(getAllCities);

            var getByIdCity = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.City_GetById.sql");
            migrationBuilder.Sql(getByIdCity);

            var getAllDepartment = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Department_GetAll.sql");
            migrationBuilder.Sql(getAllDepartment);

            var getByIdDepartment = EmbeddedResource.GetString("Uzcard.Domain.SqlScripts.Functions.Department_GetById.sql");
            migrationBuilder.Sql(getByIdDepartment);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           }
    }
}
