﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentsService : IStudentsService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IStudentRepository _studentRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentRepository"></param>
        public StudentsService(IStudentRepository studentRepository, IMapper mapper)
        {
            _studentRepository = studentRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<StudentApiModel> GetAllStudents()
        {
            var students = _studentRepository.GetAll();
            return _mapper.Map<List<StudentApiModel>>(students);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public StudentApiModel GetById(long studentId)
        {
            var student = _studentRepository.GetById(studentId);
            if (student == null)
                return null;

            return _mapper.Map<StudentApiModel>(student);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<StudentApiModel> GetObjectByFilter(string filter)
        {
            var students = _studentRepository.GetByFilter(filter);
            if (students == null || students?.Count == 0)
                return null;

            return _mapper.Map<List<StudentApiModel>>(students);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newStudent"></param>
        /// <returns></returns>
        public StudentApiModel Add(StudentApiModel newStudent)
        {
            if (newStudent == null)
                return null;

            var studentMap = _mapper.Map<Student>(newStudent);
            var student = _studentRepository.Add(studentMap);

            return _mapper.Map<StudentApiModel>(student);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="student"></param>
        /// <returns></returns>
        public bool Update(long studentId, StudentApiModel student)
        {
            if (student == null)
                return false;

            if (_studentRepository.GetById(studentId) == null)
                return false;

            var studentMap = _mapper.Map<Student>(student);
            if (_studentRepository.Update(studentMap))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public bool Delete(long studentId)
        {
            if (_studentRepository.GetById(studentId) == null)
                return false;

            if (_studentRepository.Delete(studentId))
                return true;

            return false;
        }
    }
}
