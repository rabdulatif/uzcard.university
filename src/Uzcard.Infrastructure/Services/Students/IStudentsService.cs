﻿using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStudentsService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<StudentApiModel> GetAllStudents();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        StudentApiModel GetById(long studentId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        List<StudentApiModel> GetObjectByFilter(string filter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newStudent"></param>
        /// <returns></returns>
        StudentApiModel Add(StudentApiModel newStudent);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="student"></param>
        /// <returns></returns>
        bool Update(long studentId, StudentApiModel student);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        bool Delete(long studentId);
    }
}
