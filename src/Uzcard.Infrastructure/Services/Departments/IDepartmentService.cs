﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDepartmentService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<DepartmentApiModel> GetAllObjects();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DepartmentApiModel GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        DepartmentApiModel Add(DepartmentApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(long id, DepartmentApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
