﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class DepartmentService : IDepartmentService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDepartmentRepository _departmentRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentRepository"></param>
        public DepartmentService(IDepartmentRepository departmentRepository, IMapper mapper)
        {
            _departmentRepository = departmentRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DepartmentApiModel> GetAllObjects()
        {
            var students = _departmentRepository.GetAll();
            return _mapper.Map<List<DepartmentApiModel>>(students);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DepartmentApiModel GetById(long id)
        {
            var item = _departmentRepository.GetById(id);
            if (item == null)
                return null;

            return _mapper.Map<DepartmentApiModel>(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public DepartmentApiModel Add(DepartmentApiModel obj)
        {
            if (obj == null)
                return null;

            var studentMap = _mapper.Map<Department>(obj);
            var student = _departmentRepository.Add(studentMap);

            return _mapper.Map<DepartmentApiModel>(student);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Update(long id, DepartmentApiModel item)
        {
            if (item == null)
                return false;

            if (_departmentRepository.GetById(id) == null)
                return false;

            var objMap = _mapper.Map<Department>(item);
            if (_departmentRepository.Update(objMap))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            if (_departmentRepository.GetById(id) == null)
                return false;

            if (_departmentRepository.Delete(id))
                return true;

            return false;
        }
    }
}
