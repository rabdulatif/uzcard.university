﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherSubjectService : ITeacherSubjectService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITeacherSubjectRepository _teacherSubjectRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherSubjectRepository"></param>
        public TeacherSubjectService(ITeacherSubjectRepository teacherSubjectRepository, IMapper mapper)
        {
            _teacherSubjectRepository = teacherSubjectRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TeacherSubjectApiModel> GetAllObjects()
        {
            var items = _teacherSubjectRepository.GetAll();
            return _mapper.Map<List<TeacherSubjectApiModel>>(items);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TeacherSubjectApiModel GetById(long id)
        {
            var item = _teacherSubjectRepository.GetById(id);
            if (item == null)
                return null;

            return _mapper.Map<TeacherSubjectApiModel>(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TeacherApiModel> GetHighestAndLowestMarkStudentTeacher()
        {
            var teachers = _teacherSubjectRepository.GetHighestLowestMarkStudents();
            if (teachers == null || teachers?.Count == 0)
                return null;

            return _mapper.Map<List<TeacherApiModel>>(teachers);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TeacherSubjectApiModel Add(TeacherSubjectApiModel obj)
        {
            if (obj == null)
                return null;

            var objMap = _mapper.Map<TeacherSubject>(obj);
            var item = _teacherSubjectRepository.Add(objMap);

            return _mapper.Map<TeacherSubjectApiModel>(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Update(long id, TeacherSubjectApiModel obj)
        {
            if (obj == null)
                return false;

            if (_teacherSubjectRepository.GetById(id) == null)
                return false;

            var objMap = _mapper.Map<TeacherSubject>(obj);
            if (_teacherSubjectRepository.Update(objMap))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            if (_teacherSubjectRepository.GetById(id) == null)
                return false;

            if (_teacherSubjectRepository.Delete(id))
                return true;

            return false;
        }
    }
}
