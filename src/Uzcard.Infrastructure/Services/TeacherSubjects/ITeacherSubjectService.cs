﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITeacherSubjectService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<TeacherSubjectApiModel> GetAllObjects();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TeacherSubjectApiModel GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<TeacherApiModel> GetHighestAndLowestMarkStudentTeacher();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        TeacherSubjectApiModel Add(TeacherSubjectApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(long id, TeacherSubjectApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
