﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStudentSubjectsService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<StudentSubjectApiModel> GetAllObjects();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        StudentSubjectApiModel GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        List<StudentSubjectApiModel> GetExcellentStudents(long subjectId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        StudentSubjectApiModel Add(StudentSubjectApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(long id, StudentSubjectApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
