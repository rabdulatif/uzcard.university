﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentSubjectsService : IStudentSubjectsService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IStudentSubjectRepository _studentSubjectRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentSubjectRepository"></param>
        public StudentSubjectsService(IStudentSubjectRepository studentSubjectRepository, IMapper mapper)
        {
            _studentSubjectRepository = studentSubjectRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<StudentSubjectApiModel> GetAllObjects()
        {
            var studentSubjects = _studentSubjectRepository.GetAll();
            return _mapper.Map<List<StudentSubjectApiModel>>(studentSubjects);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StudentSubjectApiModel GetById(long id)
        {
            var studentSubjects = _studentSubjectRepository.GetById(id);
            if (studentSubjects == null)
                return null;

            return _mapper.Map<StudentSubjectApiModel>(studentSubjects);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        public List<StudentSubjectApiModel> GetExcellentStudents(long subjectId)
        {
            var students = _studentSubjectRepository.GetExcellentStudents(subjectId);
            if (students == null || students?.Count == 0)
                return null;

            return _mapper.Map<List<StudentSubjectApiModel>>(students);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public StudentSubjectApiModel Add(StudentSubjectApiModel obj)
        {
            if (obj == null)
                return null;

            var objMap = _mapper.Map<StudentSubject>(obj);
            var studentSubject = _studentSubjectRepository.Add(objMap);

            return _mapper.Map<StudentSubjectApiModel>(studentSubject);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="student"></param>
        /// <returns></returns>
        public bool Update(long id, StudentSubjectApiModel student)
        {
            if (student == null)
                return false;

            if (_studentSubjectRepository.GetById(id) == null)
                return false;

            var objMap = _mapper.Map<StudentSubject>(student);
            if (_studentSubjectRepository.Update(objMap))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            if (_studentSubjectRepository.GetById(id) == null)
                return false;

            if (_studentSubjectRepository.Delete(id))
                return true;

            return false;
        }
    }
}
