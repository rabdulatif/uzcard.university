﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISubjectService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<SubjectApiModel> GetAllObjects();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SubjectApiModel GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        SubjectApiModel Add(SubjectApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(long id, SubjectApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
