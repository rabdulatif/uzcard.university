﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class SubjectService : ISubjectService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ISubjectRepository _subjectRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectRepository"></param>
        public SubjectService(ISubjectRepository subjectRepository, IMapper mapper)
        {
            _subjectRepository = subjectRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<SubjectApiModel> GetAllObjects()
        {
            var items = _subjectRepository.GetAll();
            return _mapper.Map<List<SubjectApiModel>>(items);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SubjectApiModel GetById(long id)
        {
            var item = _subjectRepository.GetById(id);
            if (item == null)
                return null;

            return _mapper.Map<SubjectApiModel>(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public SubjectApiModel Add(SubjectApiModel obj)
        {
            if (obj == null)
                return null;

            var objMap = _mapper.Map<Subject>(obj);
            var item = _subjectRepository.Add(objMap);

            return _mapper.Map<SubjectApiModel>(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Update(long id, SubjectApiModel item)
        {
            if (item == null)
                return false;

            if (_subjectRepository.GetById(id) == null)
                return false;

            var objMap = _mapper.Map<Subject>(item);
            if (_subjectRepository.Update(objMap))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            if (_subjectRepository.GetById(id) == null)
                return false;

            if (_subjectRepository.Delete(id))
                return true;

            return false;
        }
    }
}
