﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherService : ITeacherService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITeacherRepository _teacherRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherRepository"></param>
        public TeacherService(ITeacherRepository teacherRepository, IMapper mapper)
        {
            _teacherRepository = teacherRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TeacherApiModel> GetAllTeachers()
        {
            var teachers = _teacherRepository.GetAll();
            return _mapper.Map<List<TeacherApiModel>>(teachers);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        public TeacherApiModel GetById(long teacherId)
        {
            var teacher = _teacherRepository.GetById(teacherId);
            if (teacher == null)
                return null;

            return _mapper.Map<TeacherApiModel>(teacher);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<TeacherApiModel> GetByFilter(string filter)
        {
            var teachers = _teacherRepository.GetByFilter(filter);
            if (teachers == null || teachers?.Count == 0)
                return null;

            return _mapper.Map<List<TeacherApiModel>>(teachers);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TeacherApiModel Add(TeacherApiModel obj)
        {
            if (obj == null)
                return null;

            var mapModel = _mapper.Map<Teacher>(obj);
            var teacher = _teacherRepository.Add(mapModel);

            return _mapper.Map<TeacherApiModel>(teacher);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="teacher"></param>
        /// <returns></returns>
        public bool Update(long id, TeacherApiModel teacher)
        {
            if (teacher == null)
                return false;

            if (_teacherRepository.GetById(id) == null)
                return false;

            var mapModel = _mapper.Map<Teacher>(teacher);
            if (_teacherRepository.Update(mapModel))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            if (_teacherRepository.GetById(id) == null)
                return false;

            if (_teacherRepository.Delete(id))
                return true;

            return false;
        }
    }
}
