﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITeacherService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<TeacherApiModel> GetAllTeachers();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        TeacherApiModel GetById(long teacherId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        List<TeacherApiModel> GetByFilter(string filter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        TeacherApiModel Add(TeacherApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="teacher"></param>
        /// <returns></returns>
        bool Update(long id, TeacherApiModel teacher);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
