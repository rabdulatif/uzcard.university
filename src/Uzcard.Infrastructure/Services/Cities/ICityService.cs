﻿using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICityService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<CityApiModel> GetAllObjects();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CityApiModel GetById(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        CityApiModel Add(CityApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Update(long id, CityApiModel obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(long id);
    }
}
