﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Uzcard.Domain.ApiModels;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CityService : ICityService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICityRepository _cityRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityRepository"></param>
        public CityService(ICityRepository cityRepository, IMapper mapper)
        {
            _cityRepository = cityRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CityApiModel> GetAllObjects()
        {
            var cities = _cityRepository.GetAll();
            return _mapper.Map<List<CityApiModel>>(cities);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CityApiModel GetById(long id)
        {
            var citi = _cityRepository.GetById(id);
            if (citi == null)
                return null;

            return _mapper.Map<CityApiModel>(citi);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public CityApiModel Add(CityApiModel obj)
        {
            if (obj == null)
                return null;

            var objMap = _mapper.Map<City>(obj);
            var item = _cityRepository.Add(objMap);

            return _mapper.Map<CityApiModel>(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="city"></param>
        /// <returns></returns>
        public bool Update(long id, CityApiModel city)
        {
            if (city == null)
                return false;

            if (_cityRepository.GetById(id) == null)
                return false;

            var itemMap = _mapper.Map<City>(city);
            if (_cityRepository.Update(itemMap))
                return true;

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            if (_cityRepository.GetById(id) == null)
                return false;

            if (_cityRepository.Delete(id))
                return true;

            return false;
        }
    }
}
