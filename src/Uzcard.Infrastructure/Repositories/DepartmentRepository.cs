﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class DepartmentRepository : BaseRepository, IDepartmentRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public DepartmentRepository(UniversityContext context) : base(context) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Department> GetAll()
        {
            return Context.Departments.FromSqlRaw(
                "select * From Department_GetAll()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Department GetById(long id)
        {
            return Context.Departments.FromSqlRaw(
                $"SELECT * From department_getbyid('{id}')").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newDepartment"></param>
        /// <returns></returns>
        public Department Add(Department newDepartment)
        {
            newDepartment.CreatedOn = DateTime.Now;
            Context.Departments.Add(newDepartment);
            Context.SaveChanges();

            return newDepartment;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public bool Update(Department department)
        {
            var dbItem = GetById(department.DepartmentId);
            if (dbItem == null)
                return false;

            department.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(department);
            Context.SaveChanges();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public bool Delete(long departmentId)
        {
            var item = GetById(departmentId);
            item.IsDeleted = true;
            item.ModifiedOn = DateTime.Now;

            Context.Entry(item).CurrentValues.SetValues(item);
            Context.SaveChanges();

            return true;
        }
    }
}
