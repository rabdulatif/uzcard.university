﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class SubjectRepository : BaseRepository, ISubjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public SubjectRepository(UniversityContext context) : base(context) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Subject> GetAll()
        {
            return Context.Subjects.FromSqlRaw(
                "select * from subjects_getall()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Subject GetById(long id)
        {
            return Context.Subjects.FromSqlRaw(
                $"select * from subjects_getbyid('{id}')").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newSubject"></param>
        /// <returns></returns>
        public Subject Add(Subject newSubject)
        {
            newSubject.CreatedOn = DateTime.Now;
            Context.Subjects.Add(newSubject);
            Context.SaveChanges();

            return newSubject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        public bool Update(Subject subject)
        {
            var dbItem = GetById(subject.SubjectId);
            if (dbItem == null)
                return false;

            subject.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(subject);
            Context.SaveChanges();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        public bool Delete(long subjectId)
        {
            var subject = GetById(subjectId);
            subject.IsDeleted = true;
            subject.ModifiedOn = DateTime.Now;

            Context.Entry(subject).CurrentValues.SetValues(subject);
            Context.SaveChanges();

            return true;
        }
    }
}
