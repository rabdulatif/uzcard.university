﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class CityRepository : BaseRepository, ICityRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public CityRepository(UniversityContext context) : base(context){}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<City> GetAll()
        {
            return Context.Cities.FromSqlRaw(
                "select * From City_GetAll()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public City GetById(long id)
        {
            return Context.Cities.FromSqlRaw(
                $"SELECT * from city_getbyid({id})").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newCity"></param>
        /// <returns></returns>
        public City Add(City newCity)
        {
            newCity.CreatedOn = DateTime.Now;
            Context.Cities.Add(newCity);
            Context.SaveChanges();

            return newCity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public bool Update(City city)
        {
            var dbItem = GetById(city.CityId);
            if (dbItem == null)
                return false;

            city.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(city);
            Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public bool Delete(long cityId)
        {
            var item = GetById(cityId);
            item.IsDeleted = true;
            item.ModifiedOn = DateTime.Now;

            Context.Entry(item).CurrentValues.SetValues(item);
            Context.SaveChanges();

            return true;
        }
    }
}
