﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentRepository : BaseRepository, IStudentRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public StudentRepository(UniversityContext context) : base(context) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Student> GetAll()
        {
            return Context.Students.FromSqlRaw(
                "SELECT * from public.students_getall()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public Student GetById(long studentId)
        {
            return Context.Students.FromSqlRaw(
                $"SELECT * From students_getbyid('{studentId}')").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<Student> GetByFilter(string filter)
        {
            return Context.Students.FromSqlRaw(
                $"SELECT * From Students_GetObjectByFilter('{filter}')").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newStudent"></param>
        /// <returns></returns>
        public Student Add(Student newStudent)
        {
            newStudent.CreatedOn = DateTime.Now;
            Context.Students.Add(newStudent);
            Context.SaveChanges();

            return newStudent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public bool Update(Student student)
        {
            var dbItem = GetById(student.StudentId);
            if (dbItem == null)
                return false;

            student.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(student);
            Context.SaveChanges();
            
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public bool Delete(long studentId)
        {
            var student = GetById(studentId);
            student.IsDeleted = true;
            student.ModifiedOn = DateTime.Now;

            Context.Entry(student).CurrentValues.SetValues(student);
            Context.SaveChanges();

            return true;
        }
    }
}
