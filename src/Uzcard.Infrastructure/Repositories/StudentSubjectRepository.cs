﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class StudentSubjectRepository : BaseRepository, IStudentSubjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public StudentSubjectRepository(UniversityContext context) : base(context) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<StudentSubject> GetAll()
        {
            return Context.StudentSubjects.FromSqlRaw(
                "select * from studentsubject_getall()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StudentSubject GetById(long id)
        {
            return Context.StudentSubjects.FromSqlRaw(
                $"select * from studentsubject_getbyid('{id}')").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        public List<StudentSubject> GetExcellentStudents(long subjectId)
        {
            var maxMark = Context.StudentSubjects.Max(max => max.Mark);
            return Context.StudentSubjects.Take(10)
                                          .Where(s=>s.SubjectId == subjectId)
                                          .Include(s => s.Student).Include(s=>s.Subject)
                                          .Where(s => !s.IsDeleted && s.Mark == maxMark)
                                          .OrderBy(s => s.Mark).ThenBy(s => s.Student.CurrentGradeLevel)
                                          .ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public StudentSubject Add(StudentSubject obj)
        {
            obj.CreatedOn = DateTime.Now;
            Context.StudentSubjects.Add(obj);
            Context.SaveChanges();

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Update(StudentSubject obj)
        {
            var dbItem = GetById(obj.StudentSubjectId);
            if (dbItem == null)
                return false;

            obj.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(obj);
            Context.SaveChanges();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            var item = GetById(id);
            item.IsDeleted = true;
            item.ModifiedOn = DateTime.Now;

            Context.Entry(item).CurrentValues.SetValues(item);
            Context.SaveChanges();

            return true;
        }
    }
}
