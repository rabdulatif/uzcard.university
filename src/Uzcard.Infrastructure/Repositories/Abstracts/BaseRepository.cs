﻿namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseRepository
    {
        /// <summary>
        /// 
        /// </summary>
        public UniversityContext Context { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public BaseRepository(UniversityContext context)
        {
            Context = context;
        }
    }
}
