﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherRepository : BaseRepository, ITeacherRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TeacherRepository(UniversityContext context) : base(context) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Teacher> GetAll()
        {
            return Context.Teachers.FromSqlRaw(
                "SELECT * FROM teachers_getall()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        public Teacher GetById(long teacherId)
        {
            return Context.Teachers.FromSqlRaw(
                $"SELECT * FROM teachers_getbyid('{teacherId}')").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<Teacher> GetByFilter(string filter)
        {
            return Context.Teachers.FromSqlRaw(
                $"SELECT * From Teachers_GetObjectByFilter('{filter}')").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newTeacher"></param>
        /// <returns></returns>
        public Teacher Add(Teacher newTeacher)
        {
            newTeacher.CreatedOn = DateTime.Now;
            Context.Teachers.Add(newTeacher);
            Context.SaveChanges();

            return newTeacher;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacher"></param>
        /// <returns></returns>
        public bool Update(Teacher teacher)
        {
            var dbItem = GetById(teacher.TeacherId);
            if (dbItem == null)
                return false;

            teacher.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(teacher);
            Context.SaveChanges();

            return true;
        }
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        public bool Delete(long teacherId)
        {
            var teacher = GetById(teacherId);
            teacher.IsDeleted = true;
            teacher.ModifiedOn = DateTime.Now;

            Context.Entry(teacher).CurrentValues.SetValues(teacher);
            Context.SaveChanges();

            return true;
        }
    }
}
