﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Uzcard.Domain.Entities;
using Uzcard.Domain.Repositories;

namespace Uzcard.DataEFCore.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class TeacherSubjectRepository : BaseRepository, ITeacherSubjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TeacherSubjectRepository(UniversityContext context) : base(context) { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TeacherSubject> GetAll()
        {
            return Context.TeacherSubjects.FromSqlRaw(
                "select * From TeacherSubject_GetAll()").ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TeacherSubject GetById(long id)
        {
            return Context.TeacherSubjects.FromSqlRaw(
                $"SELECT * From teachersubject_getbyid('{id}')").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Teacher> GetHighestLowestMarkStudents()
        {
            var maxMark = Context.StudentSubjects.Max(s=>s.Mark);
            var minMark = Context.StudentSubjects.Min(s=>s.Mark);

            var subjectIds = Context.StudentSubjects.Take(5)
                                    .Where(s => s.Mark <= maxMark || s.Mark >= minMark)
                                    .Select(s=>s.SubjectId).ToList();

            var teacherSubjects = Context.TeacherSubjects.Include(s=>s.Teacher);
            var teachers = (from teacherSubject in teacherSubjects
                            where subjectIds.Contains(teacherSubject.SubjectId)
                            select teacherSubject.Teacher).ToList();

            return teachers;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TeacherSubject Add(TeacherSubject obj)
        {
            obj.CreatedOn = DateTime.Now;
            Context.TeacherSubjects.Add(obj);
            Context.SaveChanges();

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Update(TeacherSubject obj)
        {
            var dbItem = GetById(obj.TeacherSubjectId);
            if (dbItem == null)
                return false;

            obj.ModifiedOn = DateTime.Now;
            Context.Entry(dbItem).CurrentValues.SetValues(obj);
            Context.SaveChanges();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            var item = GetById(id);
            item.IsDeleted = true;
            item.ModifiedOn = DateTime.Now;

            Context.Entry(item).CurrentValues.SetValues(item);
            Context.SaveChanges();

            return true;
        }
    }
}
