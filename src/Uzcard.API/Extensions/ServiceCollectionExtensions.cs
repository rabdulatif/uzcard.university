﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using Uzcard.DataEFCore;
using Uzcard.DataEFCore.Repositories;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.Converters;
using Uzcard.Domain.Repositories;

namespace Uzcard.API.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static void AddApiSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "University Store API",
                    Description = "A simple example ASP.NET Core Web API",
                    Contact = new OpenApiContact
                    {
                        Name = "Abdulatif Rasulov",
                        Email = "latifrasulov10@gmail.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under MIT",
                        Url = new Uri("https://opensource.org/licenses/MIT"),
                    }
                });
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="Configuration"></param>
        public static void AddPostGreDbContext(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<UniversityContext>(
                options => options.UseNpgsql(
                    Configuration["ConnectionStrings:ApplicationConnection"]));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IStudentsService, StudentsService>();
            services.AddScoped<ITeacherService, TeacherService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<ISubjectService, SubjectService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IStudentSubjectsService, StudentSubjectsService>();
            services.AddScoped<ITeacherSubjectService, TeacherSubjectService>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<ITeacherRepository, TeacherRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<IStudentSubjectRepository, StudentSubjectRepository>();
            services.AddScoped<ITeacherSubjectRepository, TeacherSubjectRepository>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddMapperProfiles(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(config =>
            {
                config.AddProfile(new StudentProfile());
                config.AddProfile(new TeacherProfile());
                config.AddProfile(new SubjectProfile());
                config.AddProfile(new TeacherSubjectProfile());
                config.AddProfile(new StudentSubjectProfile());
                config.AddProfile(new CityProfile());
                config.AddProfile(new DepartmentProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
