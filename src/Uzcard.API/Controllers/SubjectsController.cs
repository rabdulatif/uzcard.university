﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.ApiModels;

namespace Uzcard.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ISubjectService _subjectService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectService"></param>
        public SubjectController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResponse<List<SubjectApiModel>> Get()
        {
            return _subjectService.GetAllObjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResponse<SubjectApiModel> Get(long id)
        {
            return _subjectService.GetById(id);
        }

        /// <summary>
        /// Add new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResponse<SubjectApiModel> Post([FromBody] SubjectApiModel input)
        {
            return _subjectService.Add(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update/{id}")]
        public ApiResponse<bool> Update(long id, [FromBody] SubjectApiModel input)
        {
            return _subjectService.Update(id, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public ApiResponse<bool> Delete(long id)
        {
            return _subjectService.Delete(id);
        }
    }
}
