﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.ApiModels;

namespace Uzcard.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentSubjectsController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IStudentSubjectsService _studentSubjectsService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentService"></param>
        public StudentSubjectsController(IStudentSubjectsService studentSubjectsService)
        {
            _studentSubjectsService = studentSubjectsService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResponse<List<StudentSubjectApiModel>> Get()
        {
            return _studentSubjectsService.GetAllObjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResponse<StudentSubjectApiModel> Get(long id)
        {
            return _studentSubjectsService.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet("GetExcellentStudents/{subjectId}")]
        public ApiResponse<List<StudentSubjectApiModel>> GetExcellentStudents(long subjectId)
        {
            return _studentSubjectsService.GetExcellentStudents(subjectId);
        }

        /// <summary>
        /// Add new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResponse<StudentSubjectApiModel> Post([FromBody] StudentSubjectApiModel input)
        {
            return _studentSubjectsService.Add(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update/{id}")]
        public ApiResponse<bool> Update(long id, [FromBody] StudentSubjectApiModel input)
        {
            return _studentSubjectsService.Update(id, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public ApiResponse<bool> Delete(long id)
        {
            return _studentSubjectsService.Delete(id);
        }
    }
}
