﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.ApiModels;

namespace Uzcard.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeachersController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITeacherService _teacherService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherService"></param>
        public TeachersController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
        }

        [HttpGet]
        public ApiResponse<List<TeacherApiModel>> Get()
        {
            return _teacherService.GetAllTeachers();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        public ApiResponse<TeacherApiModel> Get(long id)
        {
            return _teacherService.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("{filter}")]
        public ApiResponse<List<TeacherApiModel>> Get(string filter)
        {
            return _teacherService.GetByFilter(filter);
        }

        /// <summary>
        /// Add new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResponse<TeacherApiModel> Post([FromBody] TeacherApiModel input)
        {
            return _teacherService.Add(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update/{id}")]
        public ApiResponse<bool> Update(long id, [FromBody] TeacherApiModel input)
        {
            return _teacherService.Update(id, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public ApiResponse<bool> Delete(long id)
        {
            return _teacherService.Delete(id);
        }
    }
}
