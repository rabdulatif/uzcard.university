﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.ApiModels;

namespace Uzcard.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherSubjecsController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITeacherSubjectService _teacherSubjectService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teacherSubjectService"></param>
        public TeacherSubjecsController(ITeacherSubjectService teacherSubjectService)
        {
            _teacherSubjectService = teacherSubjectService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResponse<List<TeacherSubjectApiModel>> Get()
        {
            return _teacherSubjectService.GetAllObjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResponse<TeacherSubjectApiModel> Get(long id)
        {
            return _teacherSubjectService.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet("GetHighestAndLowestMarkStudentTeachers")]
        public ApiResponse<List<TeacherApiModel>> GetHighestAndLowestMarkStudentTeachers()
        {
            return _teacherSubjectService.GetHighestAndLowestMarkStudentTeacher();
        }

        /// <summary>
        /// Add new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResponse<TeacherSubjectApiModel> Post([FromBody] TeacherSubjectApiModel input)
        {
            return _teacherSubjectService.Add(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update/{id}")]
        public ApiResponse<bool> Update(long id, [FromBody] TeacherSubjectApiModel input)
        {
            return _teacherSubjectService.Update(id, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public ApiResponse<bool> Delete(long id)
        {
            return _teacherSubjectService.Delete(id);
        }
    }
}
