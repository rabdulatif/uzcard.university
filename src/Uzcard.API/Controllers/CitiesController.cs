﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.ApiModels;

namespace Uzcard.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICityService _cityService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityService"></param>
        public CitiesController(ICityService cityService)
        {
            _cityService = cityService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResponse<List<CityApiModel>> Get()
        {
            return _cityService.GetAllObjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResponse<CityApiModel> Get(long id)
        {
            return _cityService.GetById(id);
        }

        /// <summary>
        /// Add new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResponse<CityApiModel> Post([FromBody] CityApiModel input)
        {
            return _cityService.Add(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update/{id}")]
        public ApiResponse<bool> Update(long id, [FromBody] CityApiModel input)
        {
            return _cityService.Update(id, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public ApiResponse<bool> Delete(long id)
        {
            return _cityService.Delete(id);
        }
    }
}
