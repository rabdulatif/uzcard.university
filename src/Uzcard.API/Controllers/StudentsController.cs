﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Uzcard.DataEFCore.Services;
using Uzcard.Domain.ApiModels;

namespace Uzcard.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IStudentsService _studentService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentService"></param>
        public StudentsController(IStudentsService studentService)
        {
            _studentService = studentService;
        }

        [HttpGet]
        public ApiResponse<List<StudentApiModel>> Get()
        {
            return _studentService.GetAllStudents();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        public ApiResponse<StudentApiModel> Get(long id)
        {
            return _studentService.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("{filter}")]
        public ApiResponse<List<StudentApiModel>> Get(string filter)
        {
            return _studentService.GetObjectByFilter(filter);
        }

        /// <summary>
        /// Add new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResponse<StudentApiModel> Post([FromBody] StudentApiModel input)
        {
            return _studentService.Add(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update/{id}")]
        public ApiResponse<bool> Update(long id, [FromBody] StudentApiModel input)
        {
            return _studentService.Update(id, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public ApiResponse<bool> Delete(long id)
        {
            return _studentService.Delete(id);
        }
    }
}
